﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using CerebroService.Models;
using CerebroService.Services;

namespace CerebroService
{
    public partial class CerebroService : ServiceBase
    {
        Timer timer = new Timer();
        public CerebroService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Logger.Write("Iniciando el servicio " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 58000; //number in milisecinds  
            timer.Enabled = true;

            ApplicationSettings.initialize();
            SqlConnection destConnection = null;
            try {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = ApplicationSettings.dbserver;
                builder.InitialCatalog = ApplicationSettings.dbdatabase;
                builder.UserID = ApplicationSettings.dbusuario;
                builder.Password = ApplicationSettings.dbpassword;
                /*
                builder.DataSource = "localhost\\SQLEXPRESS01";
                builder.InitialCatalog = "dbCerebro";
                builder.UserID = "usrCerebro";
                builder.Password = "usrCerebro21.";*/

                Logger.Write("cadena de conexion " + builder.ConnectionString);
                destConnection = new SqlConnection(builder.ConnectionString);
                destConnection.Open();
                Logger.Write("Test de conexión realizado correctamente.!!!");
            }
            catch (Exception e) {
                Logger.Write("" + e);
            }
            finally {
                if (destConnection != null) {
                    destConnection.Close();                    
                }
            }
        }

        protected override void OnStop()
        {
            Logger.Write("Deteniendo el servicio cerebro " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            Logger.Write("realizando una revision " + DateTime.Now);
            if (ApplicationSettings.lhorarioejecucion != null && ApplicationSettings.lhorarioejecucion.Length > 0)
            {
                foreach (string h in ApplicationSettings.lhorarioejecucion)
                {                    
                    if (Int16.Parse(h) == DateTime.Now.Hour && DateTime.Now.Minute == 0) {
                        Logger.Write("hora de ejecucion: " + h);
                        Logger.Write("realizando una ejecucion " + DateTime.Now);
                        RevisaEstado.EjecutaRevision();
                    }
                }
            }
            
        }
        
    }
}
