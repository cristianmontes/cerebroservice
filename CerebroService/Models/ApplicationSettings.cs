﻿using CerebroService.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace CerebroService.Models
{
    class ApplicationSettings
    {
        public static string ldap { get; set; }
        public static string ServidorCorreo { get; set; }
        public static string CorreoDefault { get; set; }
        public static string Puerto { get; set; }
        public static string ssl { get; set; }

        public static string dbserver { get; set; }
        public static string dbdatabase { get; set; }
        public static string dbusuario { get; set; }
        public static string dbpassword { get; set; }

        public static string[] lhorarioejecucion { get; set; }

        public static void initialize()
        {

            try
            {
                ldap = ConfigurationManager.AppSettings.Get("cerebro.notificacion.ldap");
                ServidorCorreo = ConfigurationManager.AppSettings.Get("cerebro.notificacion.servidorcorreo");
                CorreoDefault = ConfigurationManager.AppSettings.Get("cerebro.notificacion.correodefault");
                Puerto = ConfigurationManager.AppSettings.Get("cerebro.notificacion.puerto");
                ssl = ConfigurationManager.AppSettings.Get("cerebro.notificacion.ssl");

                dbserver = ConfigurationManager.AppSettings.Get("cerebro.sqlserver.servidor");
                dbdatabase = ConfigurationManager.AppSettings.Get("cerebro.sqlserver.basedatos");
                dbusuario = ConfigurationManager.AppSettings.Get("cerebro.sqlserver.usuario");
                dbpassword = ConfigurationManager.AppSettings.Get("cerebro.sqlserver.password");

                lhorarioejecucion = ConfigurationManager.AppSettings.Get("cerebro.hora.ejecucion").Split(',');
            }
            catch (Exception ex)
            {
                Logger.Write("Error Leyendo " + ex);                
            }
        }
    }
}
