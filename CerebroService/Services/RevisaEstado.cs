﻿using CerebroService.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Text;

namespace CerebroService.Services
{
    class RevisaEstado
    {
        public static void EjecutaRevision() {
            ApplicationSettings.initialize();
            SqlConnection destConnection = null;
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = ApplicationSettings.dbserver;
                builder.InitialCatalog = ApplicationSettings.dbdatabase;
                builder.UserID = ApplicationSettings.dbusuario;
                builder.Password = ApplicationSettings.dbpassword;

                destConnection = new SqlConnection(builder.ConnectionString);
                destConnection.Open();
                
                Logger.Write("iniciando la tarea de revision de contratos por revisar");
                String sql = "SELECT id_contrato, de_rubro, descripcion, de_subgerencia, proveedor, fe_ini_contrato, " +
                    "fe_fin_contrato, cant_meses, de_estado_rev, de_severidad, de_situacion, de_moneda, " +
                    "impo_unit_con_igv, nota_reno, de_tipo_obligacion, id_subgerencia, de_categoria " +
                    "FROM tb_contrato where estado = 1 and id_estado_rev in (97, 98) and id_situacion != 84 and " + 
                    "regla_fecha_revision <= GETDATE() and fe_fin_contrato > GETDATE() " +
                    "order by id_subgerencia asc";
                Logger.Write(sql);
                List<TbContrato> listTotal = new List<TbContrato>();
                using (SqlCommand command = new SqlCommand(sql, destConnection))
                {                    
                    using (SqlDataReader reader = command.ExecuteReader())
                    {                        
                        while (reader.Read())
                        {                            
                            TbContrato tbContrato = new TbContrato();
                            tbContrato.IdContrato = reader.GetInt32(0);
                            tbContrato.DeRubro = reader.IsDBNull(1) ? "": reader.GetString(1);
                            tbContrato.Descripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                            tbContrato.DeSubgerencia = reader.IsDBNull(3) ? "" : reader.GetString(3);
                            tbContrato.Proveedor = reader.IsDBNull(4) ? "" : reader.GetString(4);
                            tbContrato.FeIniContrato = reader.IsDBNull(5) ? (DateTime?)null : reader.GetDateTime(5);
                            tbContrato.FeFinContrato = reader.IsDBNull(6) ? (DateTime?)null : reader.GetDateTime(6);
                            tbContrato.CantMeses = reader.GetInt32(7);
                            tbContrato.DeEstadoRev = reader.IsDBNull(8) ? "" : reader.GetString(8);
                            tbContrato.DeSeveridad = reader.IsDBNull(9) ? "" : reader.GetString(9);
                            tbContrato.DeSituacion = reader.IsDBNull(10) ? "" : reader.GetString(10);
                            tbContrato.DeMoneda = reader.IsDBNull(11) ? "" : reader.GetString(11);
                            tbContrato.ImpoUnitConIgv = reader.GetDecimal(12);
                            tbContrato.NotaReno = reader.IsDBNull(13) ? "" : reader.GetString(13);
                            tbContrato.DeTipoObligacion = reader.IsDBNull(14) ? "" : reader.GetString(14);
                            tbContrato.IdSubgerencia = reader.GetInt32(15);
                            tbContrato.DeCategoria = reader.IsDBNull(16) ? "" : reader.GetString(16);
                            listTotal.Add(tbContrato);
                        }                        
                    }
                }

                List<TbContrato> listEnvio = new List<TbContrato>();
                TbContrato iteminicial = new TbContrato();
                iteminicial.IdSubgerencia = -100;
                foreach (TbContrato tbContrato in listTotal) {
                    if (iteminicial.IdSubgerencia != tbContrato.IdSubgerencia)
                    {
                        //1. Enviamos todo lo que teniamos hasta ahora
                        if (listEnvio.Count > 0)
                        {
                            procesarContratos(listEnvio, "Por Revisar", true, destConnection);
                        }
                        //2. inicializamos todo de nuevo para el sgte lote
                        listEnvio = new List<TbContrato>();
                        listEnvio.Add(tbContrato);
                    }
                    else
                    {
                        listEnvio.Add(tbContrato);
                    }
                    //asignamos el item actual
                    iteminicial = tbContrato;
                }
                if (listEnvio.Count > 0)
                {
                    procesarContratos(listEnvio, "Por Revisar", true, destConnection);
                }

                Logger.Write("iniciando la tarea de revision de contratos vencidos");
                String sqlvencido = "SELECT id_contrato, de_rubro, descripcion, de_subgerencia, proveedor, fe_ini_contrato, " +
                    "fe_fin_contrato, cant_meses, de_estado_rev, de_severidad, de_situacion, de_moneda, " +
                    "impo_unit_con_igv, nota_reno, de_tipo_obligacion, id_subgerencia, de_categoria " +
                    "FROM tb_contrato where estado = 1 and id_situacion != 84 and " +
                    "fe_fin_contrato <= GETDATE() and id_estado_rev != 100 " +
                    "order by id_subgerencia asc";
                Logger.Write(sqlvencido);
                List<TbContrato> listTotalV = new List<TbContrato>();
                using (SqlCommand command = new SqlCommand(sqlvencido, destConnection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TbContrato tbContrato = new TbContrato();
                            tbContrato.IdContrato = reader.GetInt32(0);
                            tbContrato.DeRubro = reader.IsDBNull(1) ? "" : reader.GetString(1);
                            tbContrato.Descripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                            tbContrato.DeSubgerencia = reader.IsDBNull(3) ? "" : reader.GetString(3);
                            tbContrato.Proveedor = reader.IsDBNull(4) ? "" : reader.GetString(4);
                            tbContrato.FeIniContrato = reader.IsDBNull(5) ? (DateTime?)null : reader.GetDateTime(5);
                            tbContrato.FeFinContrato = reader.IsDBNull(6) ? (DateTime?)null : reader.GetDateTime(6);
                            tbContrato.CantMeses = reader.GetInt32(7);
                            tbContrato.DeEstadoRev = reader.IsDBNull(8) ? "" : reader.GetString(8);
                            tbContrato.DeSeveridad = reader.IsDBNull(9) ? "" : reader.GetString(9);
                            tbContrato.DeSituacion = reader.IsDBNull(10) ? "" : reader.GetString(10);
                            tbContrato.DeMoneda = reader.IsDBNull(11) ? "" : reader.GetString(11);
                            tbContrato.ImpoUnitConIgv = reader.GetDecimal(12);
                            tbContrato.NotaReno = reader.IsDBNull(13) ? "" : reader.GetString(13);
                            tbContrato.DeTipoObligacion = reader.IsDBNull(14) ? "" : reader.GetString(14);
                            tbContrato.IdSubgerencia = reader.GetInt32(15);
                            tbContrato.DeCategoria = reader.IsDBNull(16) ? "" : reader.GetString(16);
                            listTotalV.Add(tbContrato);
                        }
                    }
                }

                List<TbContrato> listEnvioV = new List<TbContrato>();
                TbContrato iteminicialV = new TbContrato();
                iteminicialV.IdSubgerencia = -100;
                foreach (TbContrato tbContrato in listTotalV)
                {
                    if (iteminicialV.IdSubgerencia != tbContrato.IdSubgerencia)
                    {
                        //1. Enviamos todo lo que teniamos hasta ahora
                        if (listEnvioV.Count > 0)
                        {
                            procesarContratos(listEnvioV, "Vencidos", false, destConnection);
                        }
                        //2. inicializamos todo de nuevo para el sgte lote
                        listEnvioV = new List<TbContrato>();
                        listEnvioV.Add(tbContrato);
                    }
                    else
                    {
                        listEnvioV.Add(tbContrato);
                    }
                    //asignamos el item actual
                    iteminicialV = tbContrato;
                }
                if (listEnvioV.Count > 0)
                {
                    procesarContratos(listEnvioV, "Vencidos", false, destConnection);
                }


            }
            catch (Exception e)
            {
                Logger.Write("" + e);
            }
            finally
            {
                if (destConnection != null)
                {
                    destConnection.Close();
                }
            }
        }
        public static void procesarContratos(List<TbContrato> listaContratos, string estado, bool update, SqlConnection destConnection)
        {
            TbContrato contratoini = listaContratos[0];
            string emailDestino = "";
            Logger.Write("select correo from tb_unidad where id_unidad = "+contratoini.IdSubgerencia);
            using (SqlCommand command = new SqlCommand("select correo from tb_unidad where id_unidad = @idsubgerencia;", destConnection))
            {
                command.Parameters.AddWithValue("@idsubgerencia", contratoini.IdSubgerencia);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            emailDestino = reader.GetString(0);
                            Logger.Write("idsubgerencia: "+ contratoini.IdSubgerencia +" correo destino: "  + emailDestino);
                        }
                    }
                }
            }                

            if (!String.IsNullOrEmpty(emailDestino))
            {
                string strbodyemail = getHeaderEmail();
                strbodyemail += "<h4>Estimados señores,</h4>" +
                                "<h4>Se notifican " + listaContratos.Count + " contratos " + estado + ":</h4>" +
                                "<br/>";
                strbodyemail += "<table>" +
                                "<tr>" +
                                    "<th>Categoría</th>" +
                                    "<th>Rubro</th>" +
                                    "<th>Descripción</th>" +
                                    "<th>Sub Gerencia</th>" +
                                    "<th>Proveedor</th>" +
                                    "<th>Inicio de Contrato</th>" +
                                    "<th>Fin de Contrato</th>" +
                                    "<th>Cantidad Meses contratado</th>" +
                                    "<th>Estado Revisión</th>" +
                                    "<th>Severidad</th>" +
                                    "<th>Situación</th>" +
                                    "<th>Moneda</th>" +
                                    "<th>Importe Con IGV</th>" +
                                    "<th>Notas de Renovación</th>" +
                                    "<th>Tipo de Obligación</th>" +
                                "</tr>";

                foreach (TbContrato contrato in listaContratos)
                {
                    strbodyemail +=
                        "<tr>" +
                        "<td>" + ((contrato.DeCategoria is null) ? "" : contrato.DeCategoria) + "</td>" +
                        "<td>" + ((contrato.DeRubro is null) ? "" : contrato.DeRubro) + "</td>" +
                        "<td>" + ((contrato.Descripcion is null) ? "" : contrato.Descripcion) + "</td>" +
                        "<td>" + ((contrato.DeSubgerencia is null) ? "" : contrato.DeSubgerencia) + "</td>" +
                        "<td>" + ((contrato.Proveedor is null) ? "" : contrato.Proveedor) + "</td>" +
                        "<td>" + ((contrato.FeIniContrato is null) ? "" : ((DateTime)contrato.FeIniContrato).ToString("dd/MM/yyyy")) + "</td>" +
                        "<td>" + ((contrato.FeFinContrato is null) ? "" : ((DateTime)contrato.FeFinContrato).ToString("dd/MM/yyyy")) + "</td>" +
                        "<td>" + ((contrato.CantMeses is null) ? "" : "" + contrato.CantMeses) + "</td>" +
                        "<td>" + ((contrato.DeEstadoRev is null) ? "" : contrato.DeEstadoRev) + "</td>" +
                        "<td>" + ((contrato.DeSeveridad is null) ? "" : contrato.DeSeveridad) + "</td>" +
                        "<td>" + ((contrato.DeSituacion is null) ? "" : contrato.DeSituacion) + "</td>" +
                        "<td>" + ((contrato.DeMoneda is null) ? "" : contrato.DeMoneda) + "</td>" +
                        "<td>" + ((contrato.ImpoUnitConIgv is null) ? "" : "" + contrato.ImpoUnitConIgv) + "</td>" +
                        "<td>" + ((contrato.NotaReno is null) ? "" : contrato.NotaReno) + "</td>" +
                        "<td>" + ((contrato.DeTipoObligacion is null) ? "" : contrato.DeTipoObligacion) + "</td>" +
                        "</tr>";
                }
                strbodyemail += "</table>";
                strbodyemail += getFooterEmail();
                Logger.Write("correo: " + emailDestino + " body email => " + strbodyemail);
                enviarCorreo(emailDestino, strbodyemail);

                if (update)
                {
                    foreach (TbContrato contrato in listaContratos)
                    {   
                        //if(contrato.IdEstadoRev == Constants.CODIGO_ESTREV_SIN_ALERTAR) { 
                            string sql = "UPDATE tb_contrato SET id_estado_rev = @idEstado, de_estado_rev = @deEstado WHERE id_contrato = @idcontrato";
                            using (SqlCommand command = new SqlCommand(sql, destConnection))
                            {
                                command.Parameters.AddWithValue("@idEstado", Constants.CODIGO_ESTREV_ALERTADO);
                                command.Parameters.AddWithValue("@deEstado", Constants.DESC_ESTREV_ALERTADO);
                                command.Parameters.AddWithValue("@idcontrato", contrato.IdContrato);

                                int rowsAffected = command.ExecuteNonQuery();
                                Logger.Write(rowsAffected + " fila(s) actualizadas");
                            }
                        //}
                    }
                }
            }
        }

        public static void enviarCorreo(String correoDestino, string htmlContent)
        {
            try
            {
                var FROM = ApplicationSettings.CorreoDefault;
                var SERVIDOR = ApplicationSettings.ServidorCorreo;
                var PUERTO = ApplicationSettings.Puerto;
                var SSL = ApplicationSettings.ssl;

                Logger.Write("FROM " + FROM);
                Logger.Write("SERVIDOR " + SERVIDOR);
                Logger.Write("PUERTO " + PUERTO);
                Logger.Write("SSL " + SSL);

                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServer.Certificate);

                var correo = new MailMessage();
                correo.From = new MailAddress(FROM);
                correo.To.Add(correoDestino);
                correo.Subject = "Notificaciones Cerebro";
                correo.Body = htmlContent;
                correo.IsBodyHtml = true;
                correo.Priority = MailPriority.Normal;

                SmtpClient smtp = new SmtpClient();
                smtp.Credentials = new NetworkCredential();
                smtp.Credentials = new NetworkCredential(FROM, "");
                smtp.Host = SERVIDOR;
                smtp.Port = Convert.ToInt32(PUERTO);
                smtp.EnableSsl = Convert.ToBoolean(SSL);

                //Envia correo
                smtp.Send(correo);

                Logger.Write("Correo enviado");
            }
            catch (Exception e)
            {
                Logger.Write("Correo NO enviado");
                Logger.Write(e.ToString());
                Logger.Write(e.Message);
            }
        }


        public static string getHeaderEmail()
        {
            return "<html xmlns = 'http://www.w3.org/1999/xhtml'>" +
                "<head runat = 'server'>" +
                "<meta http-equiv = 'Content-Type' content = 'text/html; charset=utf-8'/>" +
                "<title></title>" +
                "<style type = 'text/css'>" +
                "/* Cambios sobre la propia tabla */" +
                "table {" +
                "    border-collapse:collapse;" +
                "    border: 1px solid #CCC;" +
                "}" +
                "/* Espacio de relleno en celdas y cabeceras */" +
                "td, th {" +
                "    padding: 10px;" +
                "}" +
                "/* Modificación de estilos en cabeceras */" +
                "th {" +
                "    background:#337DFF;" +
                "    color:#FFF;" +
                "}" +
                "/* Modificación de estilos en celdas */" +
                "td {" +
                "    text-align:center;" +
                "    border: 1px solid #111;" +
                "    color:#333;" +
                "    font-size:10px;" +
                "}" +
                "p.MsoNormal {" +
                "    margin-bottom:.0001pt;" +
                "    font-size:11.0pt;" +
                "    font-family:'Calibri',sans-serif;" +
                "    margin-left: 0cm;" +
                "    margin-right: 0cm;" +
                "    margin-top: 0cm;" +
                "}" +
                "</style> " +
            "</head>" +
            "<body>" +
            "<form id='form1' runat='server'>" +
                "<div>" +
                    "<br/>";
        }

        public static string getFooterEmail()
        {
            return "<br/>" +
                    "</div>" +
                    "</form>" +
                    "</body>" +
                    "</html>";
        }
    }
}
