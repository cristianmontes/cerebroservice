﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CerebroService.Services
{
    class Constants
    {
        public const int CODIGO_ESTREV_ALERTADO = 98;
        public const string DESC_ESTREV_ALERTADO = "Alertado";
        public const int CODIGO_ESTREV_SIN_ALERTAR = 97;
    }
}
